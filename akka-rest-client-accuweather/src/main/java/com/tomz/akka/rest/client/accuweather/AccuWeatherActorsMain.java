package com.tomz.akka.rest.client.accuweather;

import java.io.IOException;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class AccuWeatherActorsMain {

	public static void main(String[] args) throws IOException {
		final String apiKey = new AccuWeatherConfigsLoader().getApiKey();

		ActorSystem system = ActorSystem.create("AccuWeatherActorSystem");

	    ActorRef accuWeatherActor = system.actorOf(Props.create(AccuWeatherActor.class), "AccuWeatherActor");
	    ActorRef accuWeatherCurrTemperatureActor = system.actorOf(Props.create(AccuWeatherCurrentTemperatureActor.class, apiKey, accuWeatherActor), "AccuWeatherActorTester");

	    accuWeatherCurrTemperatureActor.tell(new AccuWeatherGeoRequest(apiKey, "44.5075","11.3514"), null);
	}
	
}
