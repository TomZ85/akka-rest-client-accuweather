package com.tomz.akka.rest.client.accuweather;

public class AccuWeatherCurrentConditionResponse {
	private final String temperature;
	
	public AccuWeatherCurrentConditionResponse(String temperature) {
		this.temperature = temperature;
	}

	public String getTemperature() {
		return temperature;
	}
}
