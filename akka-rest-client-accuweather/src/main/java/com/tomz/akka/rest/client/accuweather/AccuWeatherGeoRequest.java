package com.tomz.akka.rest.client.accuweather;

public class AccuWeatherGeoRequest {
	private String apiKey;
	private String lat;
	private String lon;

	public AccuWeatherGeoRequest(String apiKey, String lat, String lon) {
		this.apiKey = apiKey;
		this.lat = lat;
		this.lon = lon;
	}

	public String getApiKey() {
		return apiKey;
	}
	public String getLat() {
		return lat;
	}
	public String getLon() {
		return lon;
	}
	
	@Override
	public String toString() {
		return "AccuWeatherGeoRequest [apiKey=" + apiKey + ", lat=" + lat + ", lon=" + lon + "]";
	}

}