package com.tomz.akka.rest.client.accuweather;

public class AccuWeatherCurrentConditionRequest {
	private String apiKey;
	private String locationKey;

	public AccuWeatherCurrentConditionRequest(String apiKey, String locationKey) {
		this.apiKey = apiKey;
		this.locationKey = locationKey;
	}

	public String getApiKey() {
		return apiKey;
	}
	public String getLocationKey() {
		return locationKey;
	}

	@Override
	public String toString() {
		return "AccuWeatherCurrentConditionRequest [apiKey=" + apiKey + ", locationKey=" + locationKey + "]";
	}
	
}