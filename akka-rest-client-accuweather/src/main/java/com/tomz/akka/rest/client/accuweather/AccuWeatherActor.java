package com.tomz.akka.rest.client.accuweather;

import java.text.MessageFormat;
import java.util.function.Function;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tomz.akka.rest.client.utils.AkkaRESTClientUtils;
import com.tomz.akka.rest.client.utils.AkkaReferencies;

import akka.actor.UntypedActor;

public class AccuWeatherActor extends UntypedActor {
	private Logger logger = LoggerFactory.getLogger(AccuWeatherActor.class);
	private static String URL_CURRCONDITIONS = "http://dataservice.accuweather.com/currentconditions/v1/{1}?apikey={0}";
	private static String URL_GEO = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={0}&q={1}%2C{2}";

	@Override
	public void onReceive(Object message) throws Throwable {
		logger.debug("Request Arrived!! " + message);
		
		final AkkaReferencies akkaReferencies = new AkkaReferencies(getContext().system(), getSender(), getSelf());
		
		if (message instanceof AccuWeatherGeoRequest) {
			AccuWeatherGeoRequest request = (AccuWeatherGeoRequest) message;
			getLocationKeyFromGeo(akkaReferencies, request.getApiKey(), request.getLat(), request.getLon());
		} else if (message instanceof AccuWeatherCurrentConditionRequest) {
			AccuWeatherCurrentConditionRequest request = (AccuWeatherCurrentConditionRequest) message;
			getCurrentCondition(akkaReferencies, request.getApiKey(), request.getLocationKey());
		} else {
			unhandled(message);
		}
	}

	private void getLocationKeyFromGeo(AkkaReferencies akkaReferencies, String apiKey, String lat, String lon) {
		String url = MessageFormat.format(URL_GEO, apiKey, lat, lon);
		AkkaRESTClientUtils.callRESTService(akkaReferencies, url, new Function<String, AccuWeatherGeoResponse>() {

			@Override
			public AccuWeatherGeoResponse apply(String entityString) {
				String key = (String) new JSONObject(entityString).get("Key");
				return new AccuWeatherGeoResponse(key);
			}
		});
	}

	private void getCurrentCondition(AkkaReferencies akkaReferencies, String apiKey, String locationKey) {
		String url = MessageFormat.format(URL_CURRCONDITIONS, apiKey, locationKey);
		AkkaRESTClientUtils.callRESTService(akkaReferencies, url, new Function<String, AccuWeatherCurrentConditionResponse>() {

			@Override
			public AccuWeatherCurrentConditionResponse apply(String entityString) {
				JSONArray key = new JSONArray(entityString);
				String temp = ((JSONObject)((JSONObject)((JSONObject)key.get(0)).get("Temperature")).get("Metric")).get("Value").toString();
				return new AccuWeatherCurrentConditionResponse(temp);
			}
		});
	}
}
