package com.tomz.akka.rest.client.accuweather;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class AccuWeatherCurrentTemperatureActor extends UntypedActor {
	private Logger logger = LoggerFactory.getLogger(AccuWeatherCurrentTemperatureActor.class);
	private final String apiKey;
	private final ActorRef accuWeatherRef;

	public AccuWeatherCurrentTemperatureActor(String apiKey, ActorRef accuWeatherRef) {
		this.apiKey = apiKey;
		this.accuWeatherRef = accuWeatherRef;//TODO discover anzichè ref ??
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof AccuWeatherGeoRequest) {
			AccuWeatherGeoRequest request = (AccuWeatherGeoRequest) message;
			logger.debug("Loc Request " + request.getLat() + "," + request.getLon());
			
			accuWeatherRef.tell(new AccuWeatherGeoRequest(apiKey, request.getLat(), request.getLon()), getSelf());
		} else if (message instanceof AccuWeatherGeoResponse) {
			AccuWeatherGeoResponse response = (AccuWeatherGeoResponse) message;
			logger.debug("Loc key " + response.getLocationKey());
			
			accuWeatherRef.tell(new AccuWeatherCurrentConditionRequest(apiKey, response.getLocationKey()), getSelf());
		} else if (message instanceof AccuWeatherCurrentConditionResponse) {
			AccuWeatherCurrentConditionResponse response = (AccuWeatherCurrentConditionResponse) message;
			logger.debug("Current temperature " + response.getTemperature() + " °c");
			
			getContext().system().terminate();
		} else {
			logger.debug("boh ..." + message);
			unhandled(message);
		}
	}
}