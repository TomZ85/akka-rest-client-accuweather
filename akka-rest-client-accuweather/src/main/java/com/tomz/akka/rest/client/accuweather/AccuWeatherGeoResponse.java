package com.tomz.akka.rest.client.accuweather;

public class AccuWeatherGeoResponse {
	private final String locationKey;
	
	public AccuWeatherGeoResponse(String locationKey) {
		this.locationKey = locationKey;
	}

	public String getLocationKey() {
		return locationKey;
	}
}
