package com.tomz.akka.rest.client.accuweather;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AccuWeatherConfigsLoader {

	public String getApiKey() throws IOException {
		final InputStream inStream = getClass().getClassLoader().getResourceAsStream("private.properties");
		final Properties propertiesConf = new Properties();
		propertiesConf.load(inStream);

		return propertiesConf.getProperty("api.key");
	}

}
